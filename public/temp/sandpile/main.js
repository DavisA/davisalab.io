var complete = false;
const COLUMNS = 200;
const ROWS = 200;
var canvas;
var ctx;
var piles;
var interval;


function main(){
    canvas = document.getElementById("canvas");
    canvas.width = COLUMNS;
    canvas.height = ROWS;

    ctx = canvas.getContext("2d");

    piles = array2d(COLUMNS, ROWS);
    piles[COLUMNS/2][ROWS/2] = 10000;
    interval = setInterval(action, 0.01);
    

    // while(!complete){
    //     topple();
    //     //renderSand();
    // }
    renderSand();


}


function getColor(value){
    switch(value){
        case 0:
            return "white";
            break;
        case 1:
            return "red";
            break;
        case 2:
            return "blue";
            break;
        case 3:
            return "green";
            break;
        default:
            return "black";
    }
    

}


function action(){
    for(let i = 0; i < 5; i++){
        topple();
    }
        
    renderSand();

    if(piles[COLUMNS/2][ROWS/2] < 4){
        console.log(interval);
        clearInterval(interval);
    }
 
    

    
}

function renderSand(){

    for(let x = 0; x < COLUMNS; x++){
        for(let y = 0; y < ROWS; y++){
            ctx.fillStyle = getColor(piles[x][y]);
            ctx.fillRect(x,y,1,1);
        }
    }
}


function topple(){
    complete = true;
    for(let x = 1; x < COLUMNS - 1; x++){
        for(let y = 1; y < ROWS -1; y++){
            if(piles[x][y] >= 4){
                piles[x+1][y]++;
                piles[x-1][y]++;
                piles[x][y+1]++;
                piles[x][y-1]++;
                piles[x][y]-= 4;
                complete = false;
            }





        }

    }



}





function array2d(x,y){
    var array = [];
    for(let i = 0; i < x; i++){
        array.push(new Array(y).fill(0));
    }

    return array;
}







window.onload = main;
function bfs(grid, start, goal){
    console.log(start.x + " " + start.y);
    console.log(goal.x + " " + goal.y);

    //just something to see how many loops are required
    var iterations = 0;
    //
    console.log("bfs");
    document.getElementById("output").innerHTML += "search type: Breadth First Search <br>";

    var root = grid[start.x][start.y];
    var goal = grid[goal.x][goal.y]
    //var goal = new Cell(50,50);
    var frontier = new Queue();
    var closed = [];

    closed.push(root);
    frontier.enqueue(root);
    draw_grid(root, goal); 
    //goal.fill("red");
    //root.fill("purple");




    var time = setInterval( () =>{
        iterations++;
        //while(!frontier.isEmpty()){
        if(!frontier.isEmpty()){
            let current = frontier.dequeue();

            // console.log("Current:");
            // console.log(current);
            current.fill("blue");
            if(current == goal){

                console.log("Interations required: " + iterations);
                clearInterval(time);
                print_route(current);
                unvisit(grid);
                return current;
            }

            let neighbors = get_valid_neighbors(grid, current);
            for(let i = 0; i < neighbors.length; i++){
                if(!closed.includes(neighbors[i])){
                    closed.push(neighbors[i]);
                    neighbors[i].parent = current;
                    frontier.enqueue(neighbors[i]);
                    //maybe?
                    //neighbors[i].visited = true;
                    neighbors[i].fill("green");
                }
            }
            root.fill("purple");
        }
        else{
            console.log("Interations required: " + iterations);
            console.log("goal not found");
            clearInterval(time);
            unvisit(grid);
        }
    }, 50);
}

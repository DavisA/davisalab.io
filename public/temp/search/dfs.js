function dfs(grid, start, goal){

    //just something to see how many loops are required
    var iterations = 0;
    //
    console.log("dfs");
    document.getElementById("output").innerHTML += "search type: Depth First Search <br>";
    var stack = [];
    var root = grid[start.x][start.y];
    var goal = grid[goal.x][goal.y];
    
    var current = root;
    stack.push(current);
    draw_grid(root, goal);
    //goal.fill("red");
    //root.fill("purple");

        var time = setInterval( () =>{
            iterations++;
            //while(stack.length != 0){
            current.fill("blue");
            if(stack.length != 0){
                current = stack.pop();
                current.fill("green");

                if(current == goal){
                    console.log("Interations required: " + iterations);
                    clearInterval(time);
                    print_route(current);
                    unvisit(grid);
                    return current;
                }

                if(current.visited == false){
                    current.visited = true;
                    var neighbors = get_valid_neighbors(grid, current);
                    for(let i = 0; i < neighbors.length; i++){
                        neighbors[i].parent = current;
                        stack.push(neighbors[i]);
                    }
                    //current.fill("green");
                }
            } else{
                console.log("Interations required: " + iterations);
                console.log("no route found");
                clearInterval(time);
                unvisit(grid); 

            }
        }, 50);   
}
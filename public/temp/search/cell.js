function Cell(x, y) {

    this.x = x;
    this.y = y;

    this.blocked = false;
    this.parent = null;
    this.visited = false;

    /* Heuristic variables */
    this.g = 0;
    this.f = 0;
    this.h = 0;

    this.draw = ()=>{
        ctx.rect(this.x * CELL_WIDTH, this.y * CELL_HEIGHT, CELL_WIDTH,CELL_HEIGHT);
        ctx.stroke();

    }

    this.fill = (clr)=>{
        ctx.fillStyle=clr;
        //ctx.beginPath();
        ctx.fillRect(this.x * CELL_WIDTH, this.y * CELL_HEIGHT, CELL_WIDTH,CELL_HEIGHT);
        //ctx.fill();
        ctx.stroke();
        //ctx.closePath();
    }
}




/* Randomly sets all cells to blocked based on chance.  start and end are always unblocked */
function random_block(grid, start, goal, chance){
    for(let x = 0; x < grid.length; x++){
        for(let y = 0; y < grid[x].length; y++){
            grid[x][y].blocked = false;
            if(Math.floor((Math.random() * 100) + 1) <= chance){
               grid[x][y].blocked = true;
           }
        }
    }

    start.blocked = false;
    goal.blocked = false;
}
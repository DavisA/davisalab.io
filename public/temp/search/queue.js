function Queue(){
    this.queue = [];


    this.enqueue = (item) => {
        this.queue.push(item);


    }

    this.dequeue = () => {
        /* return null if queue is empty */
        if(this.isEmpty()){
            return null;
        }

        var item = this.peak();
        this.queue.splice(0,1);

        return item;

    }

    this.peak = ()=>{
        
        if(this.isEmpty()){
            return null;
        }
        return this.queue[0];
    }

    this.length = ()=> {
        return this.queue.length;
    }

    this.isEmpty = ()=> {
        if(this.queue.length == 0){
            return true;
        } else {
            return false;
        }
    }


}
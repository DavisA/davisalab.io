const CANVAS_WIDTH = 800;
const CANVAS_HEIGHT = 800;
const COLUMNS = 30;
const ROWS = 30;
const CELL_WIDTH = CANVAS_WIDTH / COLUMNS;
const CELL_HEIGHT = CANVAS_HEIGHT / ROWS;
var START = {
    x: COLUMNS-5,//0,
    y: 0
};
var GOAL = {
    x: COLUMNS-1,
    y: ROWS-1
};

var interval_registers;  //todo

var selected;

const BLOCK_CHANCE = 25; /* in a % */



var canvas;
var ctx;
var grid;


function main(){

    
    canvas = document.getElementById("canvas");
    canvas.width = CANVAS_WIDTH;
    canvas.height = CANVAS_HEIGHT;
    ctx = canvas.getContext("2d");
    document.addEventListener("click",onClick);

    grid = create_grid();
    random_block(grid, grid[START.x][START.y], grid[GOAL.x][GOAL.y], BLOCK_CHANCE);
    draw_grid(grid[START.x][START.y], grid[GOAL.x][GOAL.y]);
    setup_buttons();
}


function draw_blocked(){
    for(let x = 0; x < COLUMNS; x++){
        for(let y = 0; y < ROWS; y++){
            if(grid[x][y].blocked){
                grid[x][y].fill("black");
            }
    
        }
    }
}

/* After a seach has occurred.  
A user may wish to re run the seach with the same
random blocked spaces.  unvisits everything */
function unvisit(grid){
    for(let x = 0; x < COLUMNS; x++){
        for(let y = 0; y < ROWS; y++){
            grid[x][y].visited = false;
        }
    }

}

function onClick(event){
    var rect = canvas.getBoundingClientRect();
    //console.log(canvasCoordinates);
    var mouseX = event.clientX;
    var mouseY = event.clientY;

    var mouse = {
        valid: true,
        x: event.clientX - rect.left,
        y: event.clientY - rect.top
    };

    if(mouse.x  < 0 || mouse.x > canvas.width || mouse.y  < 0 || mouse.y > canvas.height){
        mouse.valid = false;
    }

    if(mouse.valid){
        if(selected){
            if(selected.blocked)
            selected.fill("black");
        else
            selected.fill("white");
        }
        
        selected = grid[Math.floor(mouse.x / CELL_WIDTH)][Math.floor(mouse.y / CELL_HEIGHT)];
        selected.fill("orange");
        
       console.log(selected);
    }
}



function setup_buttons(){
    
    document.getElementById("bfs").addEventListener("click",()=> bfs(grid,START, GOAL));
    document.getElementById("dfs").addEventListener("click",()=> dfs(grid,START, GOAL));
    
    document.getElementById("set-root").addEventListener("click", set_root);
    document.getElementById("set-goal").addEventListener("click",set_goal);
    document.getElementById("reset").addEventListener("click",()=> draw_grid());
    document.getElementById("reset-blocked").addEventListener("click",()=> reset_blocked());
    document.getElementById("astar").addEventListener("click", ()=> aStar(grid, START, GOAL));
}

function reset_blocked(){
    random_block(grid, grid[START.x][START.y], grid[GOAL.x][GOAL.y], BLOCK_CHANCE);
    draw_grid();
}



function set_goal(){
    grid[GOAL.x][GOAL.y].fill("white");
    GOAL.x = selected.x;
    GOAL.y = selected.y;
    selected.blocked = false;
    grid[GOAL.x][GOAL.y].fill("red");
    selected = undefined;
}

function set_root(){
    grid[START.x][START.y].fill("white");
    START.x = selected.x;
    START.y = selected.y;
    selected.blocked = false;
    grid[START.x][START.y].fill("purple");
    selected = undefined;

}



function calculate_direction(origin, destination){
    if(origin.x > destination.x){
        return "left;";
    }
    if(origin.x < destination.x){
        return "right;";
    }
    if(origin.y > destination.y){
        return "up;";
    }
    if(origin.y < destination.y){
        return "down;";
    }

    //else
    return null;

}

function print_route(goal){
    var route = [];
    current = goal;
    route.push(current);
    while(current.parent != null){
        current = current.parent;
        route.push(current);
    }
    for(let i = 0; i < route.length; i++){
        route[i].fill("pink");
    }

    directions = [];
    for(let i = route.length -1; i > 0; i--){
        var d = 
        directions.push(calculate_direction(route[i], route[i -1]));
    }

    var output = "";
    for(let i = 0; i < directions.length; i++){
        output += directions[i] + " ";
    }
    console.log("Required steps: " + directions.length);
    console.log(output);
    document.getElementById("output").innerHTML += "steps required: " +  directions.length + " Route: " + output + "<hr>";
    
    
}



/* returns neighbors if they haven't been visited and aren't blocked */
function get_valid_neighbors(grid, cell){
    let neighbors = get_neighbors(grid, cell);

    for(let i = neighbors.length - 1; i >= 0; i--){
        if(neighbors[i].blocked){
            neighbors[i].visited = true;
        }

        if(neighbors[i].visited){
            neighbors.splice(i, 1);

        }
    }

    return neighbors;


}

function get_neighbors(grid, cell){
    var neighbors = [];

    if(cell.y > 0 && grid[cell.x][cell.y-1]){ //above
        neighbors.push(grid[cell.x][cell.y-1]);
    }
    
    if(cell.x > 0 && grid[cell.x-1][cell.y]){ //left
        neighbors.push(grid[cell.x-1][cell.y]);
    }
    
    if(cell.x < COLUMNS -1 && grid[cell.x+1][cell.y]){ //right
        neighbors.push(grid[cell.x+1][cell.y]);
    }

    if(cell.y < ROWS -1 && grid[cell.x][cell.y+1]){ //below
        neighbors.push(grid[cell.x][cell.y+1]);
    }

    return neighbors;
}



function draw_grid(){
    ctx.clearRect(0,0,CANVAS_WIDTH, CANVAS_HEIGHT);
    for(let x = 0; x < COLUMNS; x++){
        for(let y = 0; y < ROWS; y++){
            grid[x][y].draw();
        }
    }
    draw_blocked();

    grid[START.x][START.y].fill("purple");
    grid[GOAL.x][GOAL.y].fill("red");


}


function create_grid(){
    /* Make 2d array */
    array = new Array(COLUMNS);
    for(let i = 0; i < array.length; i++){
        array[i] = new Array(ROWS);
    }

    for(let x = 0; x < COLUMNS; x++){
        for(let y = 0; y < ROWS; y++){
            array[x][y] = new Cell(x,y);
        }
    }

    return array;
}


window.onload = main;

function aStar(grid, start, goal){
    console.log("A*");

    var open = [];
    var closed = [];
    var root = grid[start.x][start.y];
    var goal = grid[goal.x][goal.y];




    open.push(root);

    

    var time = setInterval( () =>{
        if(open.length > 0){
            var current = getLowestFScore(open);
            if(current == goal){
                console.log("Success");
                clearInterval(time);
            }

            removeFromArray(open, current);
            closed.push(current);

            let neighbors = get_neighbors(grid, current);
            for(let i = 0; i < neighbors.length; i++){
                if(closed.includes(neighbors[i]) || neighbors[i].blocked){
                    continue;
                }

                let possibleGScore = current.g + 1;  //todo this should be --current.g + dist_between(current, neighbor)--
                
                
                if(!open.includes(neighbors[i])){
                    open.push(neighbors[i]);
                } else if (possibleGScore >= neighbors[i].g){
                    continue;
                }

                neighbors[i].parent = current;
                neighbors[i].g = possibleGScore;
                neighbors[i].f = neighbors[i].g + estimateCost(neighbors[i], goal);
            }

        } else{
            console.log("no route found");
            clearInterval(time);
        }
            /* Draw shit */

            for(let o of open){
                o.fill("green");
            }
    
            for(let c of closed){
                c.fill("blue");
            }
    
            print_route(current);

    }, 50);
}


function estimateCost(a, b){
    return Math.abs(a.x - b.x) + Math.abs(a.y - b.y);
}


function removeFromArray(array, item){
    var index = array.indexOf(item);
    array.splice(index, 1);
}

function getLowestFScore(open){
    var best = open[0];

    for(let i = 0; i < open.length; i++){
        if(open[i].f < best.f){
            best = open[i];
        }
    }
    return best;
}
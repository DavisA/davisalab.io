/* CONSTANTS */

const GAMESTATE = {
    HALT: "HALT",
    RUNNING: "RUNNING",
    PAUSE: "PAUSE"
}


const EMPTY = "_";

const PLAYER = {
    X: "X",
    O: "O"
}

/* Setup canvas */

var canvas = document.getElementById("canvas");
canvas.width = 600;
canvas.height = 600;
canvas.style="border:1px solid #000000;"
var canvasctx = canvas.getContext("2d");

function Game() {
    this.gamestate = GAMESTATE.RUNNING;
    this.turn = PLAYER.X;
    
    /* Initialise board as array of 9 sqares */
    
    this.board = new Array(9).fill(EMPTY);
    this.clear = () =>  {
        for(let i = 0; i < 9; i++){
            this.board[i] = EMPTY;
        }
        //console.log("clear");
    }

    this.swapTurn = () => {
        if     (this.turn == PLAYER.X)    { this.turn = PLAYER.O; }
        else if(this.turn == PLAYER.O)    { this.turn = PLAYER.X; }
    }

    this.makeTurn = (box) => {
        if(box > -1 && box < 9){
            if(this.board[box] === EMPTY){
                this.board[box] = this.turn;
                this.swapTurn();
            }
        }else{
            //console.log("invalid turn");
        }
        //console.log(box);
        //this.printBoard();
    }

    this.printBoard = () => {
        console.log("---------");
        console.log(this.board[0] + "\t" + this.board[1] + "\t" + this.board[2]);
        console.log(this.board[3] + "\t" + this.board[4] + "\t" + this.board[5]);
        console.log(this.board[6] + "\t" + this.board[7] + "\t" + this.board[8]);
        console.log("---------");
    }
}



var game;
var renderer;

function setup(){
    game = new Game();

    /* Create renderer, perform initial grid render then set to render every second */
    renderer = new Renderer(canvasctx, canvas.width, canvas.height);
    renderer.render(game.board);
    //setInterval(renderer.render,50, game.board);

    document.addEventListener("keydown",keyDown);
    document.addEventListener("click", mouseClicked);
    document.getElementById("Reset").addEventListener("click", clearGame);
}

function clearGame(){
    game.clear();
    renderer.render(game.board);
}

function mouseClicked(event){
    var rect = canvas.getBoundingClientRect();
    var mouse = {
        valid: true,
        x: event.clientX - rect.left,
        y: event.clientY - rect.top
    };

    if(mouse.x  < 0 || mouse.x > canvas.width || mouse.y  < 0 || mouse.y > canvas.height)
        mouse.valid = false;
    
    game.makeTurn(renderer.calculateBox(mouse));
    renderer.render(game.board);
}

function keyDown(key){
    /* keycodes 1 = 49, 2 = 50 etc */
    let turn = key.keyCode - 49;
    // console.log(turn);
    if (turn >= 0 && turn < 9){
        game.makeTurn(turn);
        //game.printBoard();
        renderer.render(game.board);

    }
    
    if (key.keyCode == 32){
        //console.clear();
        clearGame();
        //console.log("space");
        //game.printBoard();
    }
}





window.onload = setup;

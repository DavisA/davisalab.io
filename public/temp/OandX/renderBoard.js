function Renderer(canvasCtx, w, h){
  this.canvasCtx = canvasCtx;
  var dimensions = {
      w: w,
      h: h
  };

  const grid = dimensions.w / 3;
  const radius = grid * 0.3;
  const line_length = grid / 2;
  const line_width = 5;


  this.renderGrid = () => {
    this.canvasCtx.fillStyle = "black";
    /* Vertical lines */
    this.canvasCtx.fillRect(grid, 0, line_width, dimensions.h);
    this.canvasCtx.fillRect((grid *2), 0, line_width, dimensions.h);
    /* Horizontal lines */
    this.canvasCtx.fillRect(0, grid, dimensions.w, line_width);
    this.canvasCtx.fillRect(0, grid * 2, dimensions.w, line_width);
  }




  /* Convert location eg (2,2) to the 
  correct pixel location to allow drawing on canvas
  */
  var convertTopoint2d = (index) => {
    loc = {
      x: index % 3 + 1,
      y: Math.floor(index /3) + 1
    }
    loc.x = loc.x * grid - (grid /2);
    loc.y = loc.y * grid - (grid /2);
    return loc;
  }

  /* 
   * Pass the X and O rendering functions
   * A grid location eg x: 1..3, y: 1..3
   * They will calculate the correct location
   * On the canvas to draw
   */
  var renderX = (index) => {
    point2d = convertTopoint2d(index);

    /* centre the x/y coordinates */
    point2d.x -= grid /4;
    point2d.y -= grid /4;

    this.canvasCtx.strokeStyle = "red";
    this.canvasCtx.lineWidth = line_width * 2;
    this.canvasCtx.beginPath();
    this.canvasCtx.moveTo(point2d.x, point2d.y);
    this.canvasCtx.lineTo(point2d.x + line_length, point2d.y + line_length);
    this.canvasCtx.stroke();
  
    this.canvasCtx.beginPath();
    this.canvasCtx.moveTo(point2d.x + line_length, point2d.y);
    this.canvasCtx.lineTo(point2d.x , point2d.y + line_length);
    this.canvasCtx.stroke();
  }

  renderO = (loc) => {
    point2d = convertTopoint2d(loc);

    this.canvasCtx.beginPath();
    this.canvasCtx.fillStyle = "blue";
    this.canvasCtx.arc(point2d.x, point2d.y, radius, 0, 2*Math.PI)
    this.canvasCtx.fill();

    this.canvasCtx.beginPath();
    this.canvasCtx.fillStyle = "white";
    this.canvasCtx.arc(point2d.x, point2d.y, radius/1.5, 0, 2*Math.PI)
    this.canvasCtx.fill();
  }

  /* private helper renderer, ensures the correct symbol is rendered */
  var render = (player, loc)=>{
    if(player == "X") renderX(loc);
    if(player == "O") renderO(loc);
  }

  this.render = (game_board) => { 
    canvasCtx.clearRect(0, 0, dimensions.w, dimensions.h);
    //console.log('rendering...');
    this.renderGrid();
    for(let i = 0; i<9; i++){
      render(game_board[i], i);
    }
  }


/****** Super shit *******/
/*
 * If the mouse coordinates are valid (inside the canvas)
 * Calculate which box in the game array they would be
 */
  this.calculateBox = (mouse) =>{
    var x;
    var y;
    var index;
    if(mouse.valid){
      //console.log('valid');
      if(mouse.x < grid)      { x = 0; }
      if(mouse.x > grid && mouse.x < grid *2)   { x = 1; }
      if(mouse.x > grid*2 && mouse.x < grid *3)   { x = 2; }

      if(mouse.y < grid)      { y = 0; }
      if(mouse.y > grid && mouse.y < grid *2)   { y = 1; }
      if(mouse.y > grid*2 && mouse.y < grid *3)   { y = 2; }
      index = x + (y * 3);
      //console.log(x + "," + y + "\nIndex: " + index);
      return index;
    } else{
    //  console.log("invalid");
    }
    return -1;

  }



}